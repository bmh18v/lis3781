> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Bryan Humphries

### Project 2 Requirements:

*Three parts:*

1. Download MongoDB and setup accordingly
2. Download Atlas and connect our MongoDB to it
3. Import Data into our DB and run

#### README.md file should include the following items:

* Screenshot of at least one MongoDB shell commands
* Optional: JSON Code for the required reports

#### Assignment Screenshots:

#### *MongoDB Shell Command Example 1*
![MongoDB Example 1](img/MongoDBex1.png "MongoDB Example 1")

#### *Mongo DB Shell Command Example 2*
![MongoDB Example 2](img/MongoDBex2.png "MongoDB Example 2")





