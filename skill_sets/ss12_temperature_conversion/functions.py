def get_requirements():
    print("Temperature Conversion Program")
    print("Developer: Bryan Humphries")
    print("\nProgram Requirements:\n"
        + "1. Program converts user-entered temperatures into Farenheit or Celsius scales.\n"
        + "2. Program continues to prompt for user entry until no longer requested.\n"
        + "3. Note: upper or lower case letters permitted. Though, incorrect entries are not permitted.\n"
        + "4. Note: Program does not validate numeric data (optional requirement).\n")


def temperature_conversion():
    temperature = 0.0
    choice = ''
    type = ''

    print("Input:")

    choice = input("Do you want to convert a temperature (y or n)? ").lower()

    print("\nOutput:")

    while (choice[0] == 'y'):
        type = input(
          "Fahrenheit to Celcius? Type \"f\", or Celcius to Fahrenheit? Type \"c\": ").lower()  

        if type[0] == 'f':
            temperature = float(input("Enter temperature in Fahrenheit: "))
            temperature = ((temperature - 32)*5)/9
            print("Temperature in Celcius = " + str(temperature))
            choice = input(
                "\nDo you want to convert another temperature (y or n)?").lower()

        elif type[0] == 'c':
            temperature = float(input("Enter temperature in Celcius: "))
            temperature = (temperature * 9/5) + 32
            print("Temperature in Fahrenheit = " + str(temperature))
            choice = input(
                "\nDo you want to convert another temperature (y or n)?").lower()

        else:
            print("Incorrect entry. Please try again.")
            choice = input(
                "\nDo you want to convert another temperature (y or n)?").lower()

    print("Thank you for using out Temperature Conversion Program!")
            

