
# LIS 3781 - Advanced Database Management

## Bryan Humphries

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Butbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram, and SQL Code (optional)
5. Bitbucket repo links:
	a) this assignment and
	b) the completed tutorial


![A1 Database Business Rules](img/businessRules.png "A1 Business Rules")

#### README.md file should include the following items:

* Screenshot of A1 ERD
* Ex1. SQL Solution
* git commands w/short descriptions

> #### Git commands w/short descriptions:

1. git init - creates a new repo
2. git status - displays the state of the working directory and the staging area. 
3. git add - adds a change in the working directory to the staging area
4. git commit - commits the staged snapshot to the project history
5. git push - publish new local commits on a remote server
6. git pull - used to fetch and download content from a remote repo and immediately update the local repo to match that content.
7. git merge - merge a different branch into your active branch

#### Assignment Screenshots:

#### Screenshot of Ampps

![AMMPS Screenshot](img/ampps.png "Ammps Screenshot")

#### Screenshot of ERD

![A1 ERD Screenshot](img/a1_erd.png "A1 ERD Screenshot")


#### Screenshot of A1 Ex1
![A1 Example 1 SQL Code](img/a1_ex1.png "A1 SQL Code")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bmh18v/bitbucketstationlocations/ "Bitbucket Station Locations")


