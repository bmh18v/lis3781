> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Extensible Enterprise Solutions

## Bryan Humphries

### Assignment 5 Requirements:
* Create and populate tables using T-SQL and MS SQL Server
* Utilize MS SQL Server ERD creator for tables and relationships
* Create stored procedures

#### README.md file should include the following items:

* Screenshot of ERD from T-SQL reports
* Optional: SQL COde for the required reports
#### Assignment Screenshots:

#### ERD from the A5 Required Reports:

![A5 ERD](img/LIS3781_A5_ERD.png "A5 ERD") 







