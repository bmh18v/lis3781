> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Bryan Humphries

### Project 1 Requirements:

*Three parts:*

1. Create tables and add correct data to those tables using mySQL
2. Create a ERD that reflects the relationships that were made when creating the tables
3. SQL Code for reports

#### README.md file should include the following items:

* Screenshot of ERD
* Optional: SQL Code for required reports: [P1 SQL File](docs/lis3781_p1_solutions.sql "P1 SQL Script")

#### Assignment Screenshots:

#### Data Analysis in Terminal

![P1 ERD](img/p1_erd_lis3781.png "P1 ERD") 





