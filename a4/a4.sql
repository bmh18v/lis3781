set ANSI_Warnings ON;
GO

-- avoids error that user kept db connection open
use master;
GO

-- drop database if exists
IF EXISTS (select name FROM master.dbo.sysdatabases WHERE name = N'bmh18v')
DROP DATABASE bmh18v;
GO

-- create database
IF NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'bmh18v')
CREATE database bmh18v;
GO

use bmh18v;
GO

-- Table Person

IF object_id (N'dbo.person', N'U') is not null
drop table dbo.person;
GO

CREATE TABLE dbo.person
(
  per_id SMALLINT not null identity(1,1),
  per_ssn binary(64) NULL,
  per_salt binary(64) NULL,
  per_fname varchar(15) not null,
  per_lname varchar(30) not null,
  per_gender char(1) not null check (per_gender IN('m','f')),
  per_dob date not null,
  per_street varchar(30) not null,
  per_city varchar(30) not null,
  per_state char(2) not null default 'FL',
  per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  per_email varchar(100) null,
  per_type char(1) not null check (per_type IN('c','s')),
  per_notes varchar(255) null,
  primary key (per_id),

  -- make sure SSN's and State IDs arer unique
  CONSTRAINT ux_per_ssn unique nonclustered (per_ssn ASC)
);
GO


-- Table phone
IF OBJECT_ID (N'dbo.phone',N'U') IS NOT NULL
drop table dbo.phone ;
GO

CREATE TABLE dbo.phone
(
  phn_id smallint not null identity (1,1),
  per_id smallint not null,
  phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  phn_type char(1) not null check (phn_type IN ('h','c','w','f')),
  phn_notes varchar(255) null,
  primary key (phn_id),

  constraint fk_phone_person
  foreign key (per_id)
  references dbo.person (per_id)
  on delete cascade
  on update cascade
);
set IDENTITY_INSERT phone ON

-- Table customer
if object_id (N'dbo.customer',N'U') IS NOT NULL
DROP TABLE dbo.customer;
GO

create table dbo.customer
(
  per_id smallint not null,
  cus_balance decimal(7,2) not null check (cus_balance >= 0),
  cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
  cus_notes varchar(255) null,
  primary key (per_id),

  constraint fk_customer_person
  foreign key (per_id)
  references dbo.person (per_id)
  on delete cascade
  on update cascade
);

-- Table slsrep
if object_id (N'dbo.slsrep',N'U') IS NOT NULL
drop table dbo.slsrep;
GO

Create table dbo.slsrep
(
  per_id smallint not null,
  srp_yr_sales_goal decimal(8,2) NOT NULL check (srp_yr_sales_goal >= 0),
  srp_ytd_sales decimal (8,2) not null check (srp_ytd_sales >= 0),
  srp_ytd_comm decimal(7,2) NOT NULL check (srp_ytd_comm >= 0),
  srp_notes varchar(255),
  primary key (per_id),

  constraint fk_slsrep_person
  foreign key (per_id)
  references dbo.person (per_id)
  on delete cascade
  on update cascade
);

-- Table srp_hist
if object_id (N'dbo.srp_hist', N'U') IS NOT NULL
drop table dbo.srp_hist;
GO

create table dbo.srp_hist
(
  sht_id smallint not null identity (1,1),
  per_id smallint not null,
  sht_type char(1) not null check (sht_type IN('i','u','d')),
  sht_modified datetime not null,
  sht_modifier varchar(45) not null default system_user,
  sht_date date not null default getDate(),
  sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
  sht_yr_total_sales decimal(8,2) not null check (sht_yr_total_sales >= 0),
  sht_yr_total_comm decimal(7,2) not null check (sht_yr_total_comm >= 0),
  sht_notes varchar(255) null,
  primary key (sht_id),

  constraint fk_srp_hist_slsrep
  foreign key (per_id)
  references dbo.slsrep (per_id)
  on delete cascade
  on update cascade
);

-- Table contact
if object_id (N'dbo.contact', N'U') IS NOT NULL
drop table dbo.contact;
GO

create table dbo.contact
(
  cnt_id int not null identity(1,1),
  per_cid smallint not null,
  per_sid smallint not null,
  cnt_date datetime not null,
  cnt_notes varchar(255) null,
  primary key (cnt_id),

  constraint fk_contact_customer
  foreign key (per_cid)
  references dbo.customer (per_id)
  on delete cascade
  on update cascade,

  constraint fk_contact_slsrep
  foreign key (per_sid)
  references dbo.slsrep (per_id)
  on delete no action
  on update no action
);

-- Table [order]
-- must use delimiter [] for reserved words (e.g. order)

if object_id (N'dbo.[order]', N'U') IS NOT NULL
drop table dbo.[order];
GO

create table dbo.[order]
(
  ord_id int not null identity(1,1),
  cnt_id int not null,
  ord_placed_date DATETIME not null,
  ord_filled_date DATETIME null,
  ord_notes varchar(255) null,
  primary key (ord_id),

  constraint fk_order_contact
  foreign key (cnt_id)
  references dbo.contact (cnt_id)
  on delete cascade
  on update cascade
);

-- Table store
if object_id (N'dbo.store', N'U') IS NOT NULL
drop table dbo.store;
GO

create table dbo.store
(
  str_id smallint not null identity(1,1),
  str_name varchar(45) not null, 
  str_street varchar(30) not null,
  str_city varchar(30) not null,
  str_state char(2) not null default 'FL',
  str_zip int not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  str_phone bigint not null check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  str_email varchar(100) not null,
  str_url varchar(100) not null,
  str_notes varchar(255) null,
  primary key (str_id)
);

-- Table invoice
if object_id (N'dbo.invoice', N'U') IS NOT NULL
drop table dbo.invoice;
GO

create table dbo.invoice
(
  inv_id int not null identity(1,1),
  ord_id int not null,
  str_id smallint not null,
  inv_date DATETIME not null,
  inv_total decimal(8,2) not null check (inv_total >= 0),
  inv_paid bit not null,
  inv_notes varchar(255) null,
  primary key (inv_id),

  -- create 1:1 relationship with order by making ord_id unique
  constraint ux_ord_id unique nonclustered (ord_id ASC),

  constraint fk_invoice_order
  foreign key (ord_id)
  references dbo.[order] (ord_id)
  on delete cascade 
  on update cascade,

  constraint fk_invoice_store
  foreign key (str_id)
  references dbo.store (str_id)
  on delete cascade
  on update cascade
);

-- Table payment
if object_id (N'dbo.payment', N'U') is not null
drop table dbo.payment;
GO

create table dbo.payment
(
  pay_id int not null identity (1,1),
  inv_id int not null,
  pay_date datetime not null,
  pay_amt decimal(7,2) not null check (pay_amt >= 0),
  pay_notes varchar(255) null,
  primary key (pay_id),

  constraint fk_payment_invoice
  foreign key (inv_id)
  references dbo.invoice (inv_id)
  on delete cascade
  on update cascade
);

-- Table vendor

if object_id (N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
GO

create table dbo.vendor
(
  ven_id smallint not null identity(1,1),
  ven_name varchar(45) not null,
  ven_street varchar(30) not null,
  ven_city varchar(30) not null,
  ven_state char(2) not null default 'FL',
  ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
  ven_email varchar(100) null,
  ven_url varchar(100) null,
  ven_notes varchar(255) null,
  primary key (ven_id)
);

-- Table product
if object_id (N'dbo.product', N'U') IS NOT NULL
drop table dbo.product;
GO

create table dbo.product
(
  pro_id smallint not null identity(1,1),
  ven_id smallint not null,
  pro_name varchar(30) not null,
  pro_descript varchar(45) not null,
  pro_weight float not null check (pro_weight >= 0),
  pro_qoh smallint not null check (pro_qoh >= 0),
  pro_cost decimal(7,2) not null check (pro_cost >= 0),
  pro_price decimal(7,2) not null check (pro_price >= 0),
  pro_discount decimal(3,0) null,
  pro_notes varchar(255) null,
  primary key (pro_id),

  constraint fk_product_vendor
  foreign key (ven_id)
  references dbo.vendor (ven_id)
  on delete cascade
  on update cascade
);

-- Table product_hist
if object_id (N'dbo.product_hist', N'U') IS NOT NULL
drop table dbo.product_hist;
GO

create table dbo.product_hist
(
  pht_id int not null identity(1,1),
  pro_id smallint not null,
  pht_date datetime not null,
  pht_cost decimal(7,2) not null check (pht_cost >= 0),
  pht_price decimal(7,2) not null check (pht_price >= 0),
  pht_discount decimal(3,0) null,
  pht_notes varchar(255) null,
  primary key (pht_id),

  constraint fk_product_hist_product
  foreign key (pro_id)
  references dbo.product (pro_id)
  on delete cascade
  on update cascade
);

-- table order_line
if object_id (N'dbo.order_line', N'U') IS NOT NULL
drop table dbo.order_line;
GO

create table dbo.order_line
(
  oln_id int not null identity(1,1),
  ord_id int not null,
  pro_id smallint not null,
  oln_qty smallint not null check (oln_qty >= 0),
  oln_price decimal(7,2) not null check (oln_price >= 0),
  oln_notes varchar(255) null,
  primary key (oln_id),

  --must use delimiters [] on reserved words (e.g. order)
  constraint fk_order_line_order
  foreign key (ord_id)
  references dbo.[order](ord_id)
  on delete cascade
  on update cascade,

  constraint fk_order_line_product
  foreign key (pro_id)
  references dbo.product (pro_id)
  on delete cascade
  on update cascade
);

select * from information_schema.tables;

--converts to binary
SELECT HASHBYTES('SHA2_512', 'test');

-- length 64 bytes
select len(hashbytes('SHA2_512', 'test'));


-- Data for table person
insert into dbo.person
(per_ssn, per_salt, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
VALUES
(1,NULL,'Steve', 'Rogers','m','1923-10-03','437 Southern Drive','Rochester', 'NY', 324402222,'srogers@comcast.net','s', NULL),
(2,NULL,'Bruce', 'Wayne','m','1968-03-20','1007 Mountain Drive','Gotham', 'NY', 983208440,'bwayne@knology.net','s', NULL),
(3,NULL,'Peter', 'Parker','m','1988-09-12','20 Ingram Street','New York', 'NY', 102862341,'pparker@msn.com','s', NULL),
(4,NULL,'Jane', 'Thompson','f','1978-05-08','13563 Ocean View Drive','Seattle', 'WA', 132084409,'jthompson@gmail.com','s', NULL),
(5,NULL,'Debra', 'Steele','f','1994-07-19','543 Oak Ln','Milwaukee', 'WI', 286234178,'dsteele@verizon.net','s', NULL),
(6,NULL,'Tony', 'Smith','m','1972-05-04','332 Palm Avenue','Malibu', 'CA', 902638332,'tstark@yahoo.com','c',NULL),
(7,NULL,'Hank', 'Pymi','m','1980-08-28','2355 Brown Street','Cleveland', 'OH', 822348890,'hpym@aol.com','c', NULL),
(8,NULL,'Bob', 'Best','m','1992-02-10','4902 Avendale Avenue','Scottsdale', 'AZ', 872638332,'bbest@yahoo.com','c', NULL),
(9,NULL,'Sandra','Dole','f','1990-01-26','87912 Lawrence Ave','Atlanta', 'GA', 672348890,'sdole@gmail.com','c', NULL),
(10,NULL,'Ben', 'Avery','m','1983-12-24','6432 Thunderbird Ln','Sioux Falls', 'SD', 562638332,'bavery@hotmail.com','c', NULL),
(11,NULL,'Arthur', 'Curry','m','1972-12-15','3304 Euclid Avenue','Miami', 'FL', 342219932,'acurry@gmail.com','c', NULL),
(12,NULL,'Diana', 'Prince','f','1980-08-22','944 Green Street','Las Vegas', 'NV', 332048823,'dprice@symaptico.com','c', NULL),
(13,NULL,'Adam', 'Smith','m','1995-01-31','98435 Valencia Dr.','Gulf Shores', 'AL', 870219932,'ajurris@gmx.com','c', NULL),
(14,NULL,'Judy', 'Sleen','f','1970-03-22','56343 Rover Ct.','Billings', 'MT', 672048823,'jsleen@symaptico.com','c', NULL),
(15,NULL,'Bill', 'Neiderheim','m','1982-06-13','43567 Netherland Blvd','South Bend', 'IN', 320219932,'bneiderheim@comcast.net','c', NULL);
IF OBJECT_ID(N'dbo.CreatePersonSSN', N'P') IS NOT NULL
DROP PROC dbo.CreatePersonSSN
GO

CREATE PROC dbo.CreatePersonSSN 
AS 
BEGIN



	Declare @salt binary(64);
	DECLARE @ran_num int;
	DECLARE @ssn binary(64);
	DECLARE @x INT, @y INT;
	SET @x=1;

	SET @y=(select count(*) from dbo.person);

	while (@x <=@y)
	BEGIN
	SET @salt=CRYPT_GEN_RANDOM(64);
	SET @ran_num=FLOOR(RAND()*(999999999-111111111+1))+111111111;
	SET @ssn=HASHBYTES('SHA2_512', concat(@salt, @ran_num));

	update dbo.person
	set per_ssn=@ssn, per_salt=@salt
	where per_id=@x;

	SET @x = @x + 1;

	END;

END;
GO
exec dbo.CreatePersonSSN;





select * from dbo.person;

-- Data for table phone
insert into dbo.phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
VALUES
(1,2,'8502318890', 'w', NULL),
(2,3,'9547812365','c', NULL),
(3,4,'7869905478','h', NULL),
(4,5,'5614583621','w', NULL),
(5,2,'6782352013','h', 'do not call after 9pm');
select * from dbo.phone;

-- Data for table slsrep
INSERT INTO dbo.slsrep
(per_id, srp_yr_sales_goal,srp_ytd_sales, srp_ytd_comm, srp_notes)
VALUES
(1, 100000, 60000, 1800, NULL),
(2, 80000, 35000, 3500, NULL),
(3, 150000, 84000, 9650, 'Great salesperson!'),
(4, 125000, 87000, 15300, NULL),
(5, 98000, 43000, 8750, NULL);

select * from dbo.slsrep;

-- Data for table customer
insert into dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
VALUES
(6, 120, 14789, NULL),
(7, 98.46, 234.92, NULL),
(8, 0, 4578, 'Customer always pays on time.'),
(9, 981.73, 1672.38, 'High balance.'),
(10, 541.23, 782.57, NULL);
select * from dbo.customer;

-- Data for table contact
INSERT into dbo.contact
(per_sid, per_cid, cnt_date,cnt_notes)
VALUES
(1,6,'1999-01-01', NULL),
(2,6, '2001-09-29', NULL),
(3,7,'2002-08-15', NULL),
(2,7,'2002-09-01', NULL),
(4,7,'2004-01-05', NULL);
select * from dbo.contact;

-- Data for table order
INSERT INTO dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
VALUES
(1, '2010-11-23', '2010-12-24', NULL),
(2, '2005-03-19', '2005-07-28', NULL),
(3, '2011-07-01', '2011-07-06', NULL),
(4, '2009-12-24', '2010-01-05', NULL),
(5, '2008-09-21', '2008-11-26', NULL);
select * from dbo.[order];

-- Data for table store
Insert into dbo.store
(str_name, str_street , str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
VALUES
('Walgreens', '14567 Walnut Ln', 'Aspen', 'IL', '475315690','3127658127','info@walgreens.com','http://www.walgreens.com',NULL),
('CVS', '572 Casper Rd', 'Chicago', 'IL', '505231519','3128926534','help@cvs.com','http://www.cvs.com','Rumor of merger'),
('Lowes', '81309 Catapult Ave', 'Clover', 'WA', '802345671','9017653421','sales@lowes.com','http://www.lowes.com',NULL),
('Walmart', '14567 Walnut Ln', 'St.Louis', 'FL', '387563628','8722718923','info@walmart.com','http://www.walmart.com',NULL),
('Dollar General', '47583 Davison Rd', 'Detroit', 'MI', '482983456','3137583492','ask@dollargeneral.com','http://www.dollargeneral.com','recently sold property');
select * from dbo.store;

-- Data for table invoice
insert into dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
values
(5,1,'2001-05-03',58.32,0, NULL),
(4,1,'2006-11-11',100.59,0, NULL),
(1,1,'2010-09-16',57.34,0, NULL),
(3,2,'2011-01-10',99.32,1, NULL),
(2,3,'2008-06-24',1109.67,1, NULL);
select * from dbo.invoice;

-- Data for table vendor
INSERT into dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email , ven_url, ven_notes)
values
('Sysco','531 Dolphin Run', 'Orlando', 'FL', '344761234', '7641238543', 'sales@sysco.com', 'http://www.sysco.com', NULL),
('General Electric','100 Happy Trails Dr.', 'Boston', 'MA', '123458743', '2134569641', 'support@ge.com', 'http://www.ge.com', 'very good turnaround'),
('Cisco','300 Cisco Dr.', 'Stanford', 'OR', '872315492', '7823456723', 'cisco@cisco.com', 'http://www.cisco.com', NULL),
('Goodyear','100 Goodyear Dr.', 'Gary', 'IN', '485321956', '5784218427', 'sales@goodyear.com', 'http://www.goodyear.com', 'Competing well with Firestone.'),
('Snap-on','42185 Magneta Ave', 'Lake Falls', 'ND', '387513649', '9197345632', 'support@snapon.com', 'http://www.snap-on.com', 'good quality tools');
select * from dbo.vendor;

-- Data for table product
insert into dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
values
(1, 'hammer','', 2.5, 45, 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'),
(2, 'screwdriver','', 1.8, 120, 1.99, 3.49, NULL, NULL),
(4, 'pail','16 gallon', 2.8, 48, 3.89, 7.99, 40, NULL),
(5, 'cooking oil','peanut oil', 15, 19, 19.99, 28.99, NULL, 'gallons'),
(3, 'frying pan','', 3.5, 178, 8.45, 13.99, 50, 'Currently 1/2 price sale');
select * from dbo.product;

-- Data for table order_line
insert into dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
values
(1,2,10,8.0, NULL),
(2,3,7,9.88, NULL),
(3,4,3, 6.99, NULL),
(5,1,2,12.76,NULL),
(4,5,13,58.99, NULL);
select * from order_line;

-- Data for table payment
insert into dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
values
(5,'2008-07-01', 5.99, NULL),
(4, '2010-09-28',4.99, NULL),
(1,'2008-07-23',8.75, NULL),
(3,'2010-10-31',19.55, NULL),
(2,'2011-03-29',32.5, NULL);
select * from dbo.payment;


-- Data for table product_hist
insert into dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
values
(1, '2005-01-02 11:53:34', 4.99, 7.99, 30, 'Discounted only when purchased with screwdriver set.'),
(2, '2005-02-03 09:13:56', 1.99, 3.49, NULL, NULL),
(3, '2005-03-04 23:21:49', 3.89, 7.99, 40, NULL),
(4, '2006-05-06 18:09:04', 19.99, 28.99, NULL, 'gallons'),
(5, '2006-05-07 15:07:29', 8.45, 13.99, 50, 'Currently 1/2 price sale.');
select * from dbo.product_hist;

-- Data for table 
insert into dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
values
(1, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 110000, 11000, NULL),
(4, 'i', getDate(), SYSTEM_USER, getDate(), 150000, 175000, 17500, NULL),
(3, 'u', getDate(), SYSTEM_USER, getDate(), 200000, 185000, 18500, NULL),
(2, 'u', getDate(), ORIGINAL_LOGIN(), getDate(), 210000, 220000, 22000, NULL),
(5, 'i', getDate(), ORIGINAL_LOGIN(), getDate(), 225000, 230000, 2300, NULL);
select * from dbo.srp_hist;



