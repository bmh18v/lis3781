> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Bryan Humphries

### Assignment 4 Requirements:
* Create and populate tables using T-SQL on MS SQL Server
* Create ERD based off of the created tables
* Create stored procedures

#### README.md file should include the following items:

* Screenshot of ERD from T-SQL Reports
* Optional: SQL Code for the required reports

#### Assignment Screenshots:

#### ERD From the Required Reports:
![A4 ERD](img/A4_ERD.png "A4 ERD")






