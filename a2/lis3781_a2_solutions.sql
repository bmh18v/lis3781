drop database if exists bmh18v;
create database if not exists bmh18v;
use bmh18v;

-- Table company
drop table if exists company
create table if not exists company 
(
cmp_id INT UNSIGNED NOT NULL AUTO_INCREMENT, 
cmp_type enum('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership'), 
cmp_street varchar(30) NOT NULL, 
cmp_city VARCHAR(30) NOT NULL, 
cmp_state CHAR(2) NOT NULL, 
cmp_zip int(9) unsigned ZEROFILL NOT NULL COMMENT 'no dashes', 
cmp_phone bigint unsigned not null comment 'ssn and zip codes can be zero-filled, but not US area codes', 
cmp_ytd_sales decimal(10,2) unsigned not null comment '12,345,678,90',
cmp_email varchar(100) null, 
cmp_url varchar(100) null, 
cmp_notes varchar(255) null, 
primary key (cmp_id)
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;
SHOW WARNINGS;

INSERT INTO company
VALUES
(null,'C-Corp','282 W Circle Dr','Tallahassee', 'FL', '323030100','8502130124','12345678.00',null,null,'for profit'),
(null,'S-Corp','1502 Winding Way','Tallahassee', 'FL', '323030100','8503658745','98765431.00',null,'http://bryanhumphries.com',null),
(null,'Non-Profit-Corp','756 Drive Way','Davie', 'FL', '333280200','9548506321','77954123.00',null,'http://nonprofitwin.com',null),
(null,'LLC','9023 Snow Road','Boston', 'MA', '635690232','2063218954','12368960.00',null,null,'update tables'),
(null,'Partnership','8795 Seahawks Dr','Seattle', 'WA', '018239860','2008746532','32165498.25',null,'http://technology.com',null);



drop table if exists customer;
create table if not exists customer
(
cus_id int unsigned not null auto_increment,
cmp_id int unsigned not null,
cus_ssn binary(64) not null,
cus_salt binary(64) not null COMMENT '*only* demo purposes - do *NOT* use *salt* in the name!',
cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering'),
cus_first varchar(15) not null,
cus_last varchar(30) not null,
cus_street varchar(30) null,
cus_city varchar(30) null,
cus_state char(2) null,
cus_zip int(9) unsigned zerofill null,
cus_phone bigint unsigned not null COMMENT 'ssn and zip codes can be zero-filled, but not US area codes',
cus_email varchar(100) null,
cus_balance decimal(7,2) unsigned null COMMENT'12,345.67',
cus_tot_sales decimal(7,2) unsigned null,
cus_notes varchar(255) null,
primary key (cus_id),
UNIQUE INDEX ux_cus_ssn (cus_ssn ASC),
INDEX idx_cmp_id (cmp_id ASC),
CONSTRAINT fk_customer_company
FOREIGN KEY (cmp_id)
REFERENCES company (cmp_id)
ON DELETE NO ACTION
ON UPDATE CASCADE
)
ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

SHOW WARNINGS;

set @salt=RANDOM_BYTES(64);

INSERT INTO customer
VALUES
(null,2,unhex(SHA2(CONCAT(@salt, 000456789),512)),@salt,'Loyal','Sean','Lee','243 Starway Dr','Talahassee','FL','323030122','8506268912','SLee@gmail.com','10223.35','30650.24','loyal customer'),
(null,1,unhex(SHA2(CONCAT(@salt, 000145678),512)),@salt,'Discount','Thomas','Ta','1593 West Way','Pembroke Pines','FL','333301233','9548956214','TTa@gmail.com','9035.63','25987.32','customer discount'),
(null,4,unhex(SHA2(CONCAT(@salt, 000987654),512)),@salt,'Wandering','Valerie','Hernandez','927 North Drive','Orlando','FL','333690011','4075214587','ValH@aol.com','8055.23','28652.74',null),
(null,3,unhex(SHA2(CONCAT(@salt, 000445678),512)),@salt,'Impulse','Gabi','Leon','988 Weston Cir','Weston','FL','333260022','9548756523','GLeon@yahoo.com','3025.23','41231.85',null),
(null,5,unhex(SHA2(CONCAT(@salt, 000345678),512)),@salt,'Need-Based','Tony','Stark','132 Racing Dr','Palm Beach','FL','322010221','5612340132','TStark@gmail.com','12348.23','41230.02',null);


