> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Bryan Humphries

### Assignment 2 Requirements:

*Three parts:*

1. Create my database on the CCI Server 
2. Create two tables "company" and "customer"
3. Populate those tables with 5 records in each table

#### README.md file should include the following items:

* Screenshot of my SQL Code
* Screenshot of my populated Company and Customer tables


#### Assignment Screenshots:

| Screenshot of Company Table SQL Code: | Screenshot of Customer Table SQL Code:
| :-|:-|:-
![Company SQL Code](img/companytable.png "Company SQL Code") | ![Customer SQL Code](img/customertablecode.png "Customer SQL Code")

#### Screenshot of A2 Populated Tables

![A2 Populated Tables](img/updateda2populatedtable.png "A2 Populated Tables")
