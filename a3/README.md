> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Bryan Humphries

### Assignment 3 Requirements:

*Three parts:*

1. Login to Oracle SQL Server using Remote Labs
2. Create and populate required Oracle tables
3. Ensure tables and data forward engineer to CCI Server

#### README.md file should include the following items:

* Screenshot of my Oracle SQL Code
* Screenshot of my populated tables in the Oracle Environment
* **Optional:** SQL code for the reports
* LIS 3781 Bitbucket repo links

#### Assignment Screenshots:

#### Oracle SQL Code
##### **First Part of SQL Code**
![First SQL Code](img/sqlcodep1.png "First Part of SQL Code")

| Second Part of SQL Code | Third Part of SQL Code
| :-|:-|:-
![Second SQL Code](img/sqlcodep2.png "Second Part of SQL Code") | ![Third SQL Code](img/sqlcodep3.png "Third Part of SQL Code")

#### Oracle SQL Populated Tables

##### **Populated *Customer* Table**
![Populated Table 1](img/populatedtabld1.png "Populated Table 1")

| Populated *Commodity* Table | Populated *Order* Table
| :-|:-|:-
![Populated Table 2](img/populatedtable2.png "Populated Table 2") | ![Populated Table 3](img/populatedtable3.png "Populated Table 3")





