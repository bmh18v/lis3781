> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Bryan Humphries

### LIS 3781 Requirements:

### Assignments:

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install AMPPS
	- Provide screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket Tutorial (bitbucketstationlocations)
	- Provide git command descriptions
	

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create Database using SQL
	- Make two tables named company and customer
	- Populate those tables using my database on the CCI Server

	
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Login to Oracle SQL Developer using Remote Labs
	- Create personalized database schema
	- Create and Populate Oracle tables
	

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Create Tables in MS SQL Server
	- Fill Tables with Valid Data
	- Create ERD Utilizing MS SQL Server 
	

5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Modify and create tables in MS SQL Server
	- Insert valid data into the tables
	- Create an ERD of the tables and relationships using MS SQL Server

### Projects:

 1. [P1 README.md](p1/README.md "My P1 README.MD file")
	- Create tables and fill them with data in MySQL
	- Create an ERD that reflects the tables made

2. [P2 README.md](p2/README.md "My P2 README.md file")
	- Download MongoDB and setup accordingly
	- Download Atlas and connect MongoDB
	- Run MongoDB shell commands and JSON Code for required reports
	
